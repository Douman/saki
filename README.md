# Saki

[![Build](https://gitlab.com/Douman/saki/badges/master/build.svg)](https://gitlab.com/Douman/saki/pipelines)

[Downloads](https://gitlab.com/Douman/saki/tags)

IRC/Discord bridge

## Usage

Currently you only need to start binary in the same directory with configuration
file `saki.toml`.
You can find example in repository.

### Commands

All commands exist on both IRC and Discord ends.

- `~restart` - Restart IRC or Discord end


## Config

Simple toml configuration with following sections

### discord

Describes necessary configuration for Discord end.
Currently you only need to specify `token` for bot to use

### irc

Full blown configuration of [irc crate](https://github.com/aatxe/irc)

### mapping

Describes how to route messages between IRC and Discord

```toml
[mapping.discord]
666 = "#Saki_test"

[mapping.irc]
"#Saki_test" = 666
```

`mapping.discord` section describes routes from Discord(Channel/Server with ID=666) toward IRC(Channel `#Saki_test`)

`mapping.irc` section describes routes from IRC(Channel `#Saki_test`) toward Discord(Channel/Server with ID=666)
