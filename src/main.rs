#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_panic;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;
extern crate actix;

mod utils;
mod actors;
mod config;
mod rt;

fn run_system(config: config::Config) {
    let system = actix::System::new("saki");

    let mapping = config.mapping;
    let mapping_discord = mapping.discord;
    let mapping_irc = mapping.irc;

    let irc_config = config.irc;
    let irc = actix::Supervisor::start(move |_| actors::irc::Irc::new(irc_config, mapping_irc));
    let irc_sub = irc.clone().recipient::<actors::message::ChatMessage>();

    let discord_config = config.discord;
    let discord = actix::Supervisor::start(move |_| actors::discord::Discord::new(discord_config, mapping_discord));
    let discord_sub = discord.clone().recipient::<actors::message::ChatMessage>();

    discord.do_send(actors::message::AddSub(irc_sub));
    irc.do_send(actors::message::AddSub(discord_sub));

    let _ = system.run();
}

fn main() {
    rt::ssl::init();
    rt::log::init();

    let config = match config::Config::load() {
        Ok(config) => config,
        Err(error) => {
            use std::process::exit;
            eprintln!("Unable to loadd config: {}", error);
            exit(1)
        },
    };

    run_system(config);
}
