extern crate irc;
extern crate toml;

use std::collections::HashMap;
use std::env;
use std::fs;
use std::io;
use std::path::PathBuf;

const NAME: &'static str = "saki.toml";

/// Retrieves path to configuration.
fn get_config() -> PathBuf {
    let mut result = env::current_exe().unwrap();

    result.set_file_name(NAME);

    result
}

pub type MappingDiscord = HashMap<String, String>;
pub type MappingIrc = HashMap<String, u64>;

#[derive(Deserialize, Clone, Debug)]
pub struct Mapping {
    #[serde(default)]
    pub discord: MappingDiscord,
    #[serde(default)]
    pub irc: MappingIrc,
}

#[derive(Deserialize, Clone)]
pub struct Discord {
    pub token: String,
}

#[derive(Deserialize, Clone)]
pub struct Config {
    pub irc: irc::client::data::config::Config,
    pub discord: Discord,
    pub mapping: Mapping,
}

impl Config {
    #[inline]
    pub fn load() -> io::Result<Self> {
        use std::io::Read;

        let path = get_config();
        let mut file = fs::File::open(path)?;
        let file_len = file.metadata()?.len() as usize;
        let mut buffer = String::with_capacity(file_len);
        file.read_to_string(&mut buffer)?;

        toml::from_str(&buffer).map_err(|error| io::Error::new(io::ErrorKind::Other, error))
    }
}
