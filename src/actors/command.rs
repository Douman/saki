extern crate regex;

pub enum Command {
    Shutdown,
}

impl Command {
    pub fn from_str(text: &str) -> Option<Command> {
        lazy_static! {
            static ref EXTRACT_CMD: regex::Regex = regex::Regex::new("^\\s*\\~([^\\s]*)(\\s+(.+))*").unwrap();
        }

        const CMD_IDX: usize = 1;
        // const ARG_IDX: usize = 3;

        if let Some(captures) = EXTRACT_CMD.captures(text) {
            let cmd = captures.get(CMD_IDX);
            let cmd = cmd.map(|cmd| cmd.as_str());

            match cmd {
                Some("restart") => Some(Command::Shutdown),
                _ => None,
            }
        } else {
            None
        }
    }
}
