//! Describes actors messages
extern crate actix;

#[derive(Clone)]
pub enum ChatTarget {
    Text(String),
    Guid(u64),
}

impl ChatTarget {
    pub fn text(&self) -> &str {
        match self {
            ChatTarget::Text(text) => text.as_ref(),
            ChatTarget::Guid(_) => unreachable!(),
        }
    }

    pub fn guid(&self) -> u64 {
        match self {
            ChatTarget::Guid(id) => *id,
            ChatTarget::Text(text) => u64::from_str_radix(&text, 10).expect("to parse GUID"),
        }
    }
}

#[derive(Clone)]
/// Common message for IRC and Discord
pub struct ChatMessage {
    pub target: ChatTarget,
    pub from: String,
    pub content: String,
}

impl actix::Message for ChatMessage {
    type Result = ();
}

pub struct AddSub(pub actix::Recipient<ChatMessage>);

impl actix::Message for AddSub {
    type Result = ();
}

pub struct StopSystem;
impl actix::Message for StopSystem {
    type Result = ();
}
