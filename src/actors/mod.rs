extern crate actix;
extern crate futures;

use self::futures::Future;

pub mod command;
pub mod discord;
pub mod irc;
pub mod message;

pub use self::discord::Discord;
pub use self::irc::Irc;

const MAX_NUM_SUB: usize = 1;
/// Describes sub
#[derive(Default)]
struct Subscribers {
    list: [Option<actix::Recipient<message::ChatMessage>>; MAX_NUM_SUB],
    num: usize,
}

impl Subscribers {
    pub fn push(&mut self, sub: actix::Recipient<message::ChatMessage>) {
        assert!(self.num < MAX_NUM_SUB);
        self.list[self.num] = Some(sub);
        self.num += 1;
    }

    // pub fn reset(&mut self) {
    //    for sub in self.list.iter_mut() {
    //        sub.take();
    //    }
    // }

    pub fn broadcast(&self, msg: message::ChatMessage) {
        for idx in 0..self.num {
            match self.list[idx].as_ref() {
                Some(sub) => actix::spawn(sub.send(msg.clone()).map_err(|error| error!("Bridge is broken: {}", error))),
                None => unreachable!(),
            }
        }
    }
}
