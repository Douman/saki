extern crate actix;
extern crate futures;
extern crate irc;
extern crate regex;

use std::time;

const START_TIMEOUT: time::Duration = time::Duration::from_millis(1_500);
const KICK_TIMEOUT: time::Duration = time::Duration::from_millis(500);

use super::command;
use super::message;
use config;
use utils::wait_queue;

use self::actix::prelude::*;
use self::futures::Stream;
use self::irc::client::data::config::Config as IrcConfig;
use self::irc::client::ext::ClientExt;
use self::irc::client::{Client, IrcClient};
use self::irc::error::IrcError;
use self::irc::proto::message::Message as InnerIrcMessage;

static mut QUEUE: Option<wait_queue::Queue<message::ChatMessage>> = None;

pub struct Irc {
    client: Option<IrcClient>,
    config: IrcConfig,
    subs: super::Subscribers,
    mapping: config::MappingIrc,
    wait_queue: &'static mut wait_queue::Queue<message::ChatMessage>
}

impl Irc {
    pub fn new(config: IrcConfig, mapping: config::MappingIrc) -> Self {
        let wait_queue;
        unsafe {
            if QUEUE.is_none() {
                QUEUE = Some(wait_queue::Queue::new());
            }
            wait_queue = QUEUE.as_mut().unwrap();
        }

        Self {
            client: None,
            config,
            subs: super::Subscribers::default(),
            mapping,
            wait_queue
        }
    }

    fn shutdown_client(&mut self) {
        if let Some(client) = self.client.take() {
            let _ = client.send(irc::proto::command::Command::QUIT(None));
        }
    }

    fn send_msg(client: &IrcClient, msg: message::ChatMessage) -> Result<(), (message::ChatMessage, IrcError)> {
        client.send_privmsg(&msg.target.text(), &format!("<{}> {}", msg.from, msg.content)).map_err(|error| (msg, error))
    }
}

impl Actor for Irc {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        info!("IRC: starting");

        let irc = IrcClient::new_future(self.config.clone())
            .into_actor(self)
            .map_err(|error, _act, ctx| {
                error!("IRC: Unable to connect to server. Error: {}", error);
                ctx.run_later(START_TIMEOUT, |_, ctx| ctx.stop());
            })
            .map(|(client, future), act, ctx| {
                info!("IRC: Connected");
                let future = future.into_actor(act).map_err(|error, _act, ctx| {
                    error!("IRC: Runtime error: {}", error);
                    ctx.stop();
                });
                ctx.spawn(future);

                client.send_cap_req(&[irc::proto::caps::Capability::MultiPrefix]).expect("To send caps");
                client.identify().expect("To identify");

                let stream = client.stream().map(|msg| IrcMessage(msg));
                Self::add_stream(stream, ctx);

                for msg in act.wait_queue.drain() {
                    match Self::send_msg(&client, msg) {
                        Ok(_) => (),
                        Err((_, error)) => warn!("IRC: Unable to send waiting message. Error: {}", error),
                    }
                }

                act.client = Some(client);
            });

        ctx.spawn(irc);
    }

    fn stopped(&mut self, _ctx: &mut Context<Self>) {
        self.shutdown_client();
    }
}

impl Supervised for Irc {
    fn restarting(&mut self, _: &mut Context<Self>) {
        info!("IRC: Restarting...");
        self.client.take();
    }
}

#[derive(Debug)]
struct IrcMessage(pub InnerIrcMessage);

impl Message for IrcMessage {
    type Result = Result<(), IrcError>;
}

impl StreamHandler<IrcMessage, IrcError> for Irc {
    fn error(&mut self, error: IrcError, _ctx: &mut Self::Context) -> actix::Running {
        warn!("IRC: IO error: {}", error);
        actix::Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        warn!("IRC: Connection is closed");
        ctx.stop();
    }

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        use self::irc::proto::command::Command;

        debug!("IRC: message={:?}", msg);

        let client = match self.client.as_ref() {
            Some(client) => client,
            None => unreachable!(),
        };

        let msg = msg.0;
        let from = msg.prefix.as_ref().map(|prefix| &prefix[..prefix.find('!').unwrap_or(0)]);
        match msg.command {
            Command::PRIVMSG(target, msg) => {
                debug!("from {:?} to {}: {}", from, target, msg);

                let from = match from {
                    Some(from) => from.to_owned(),
                    None => {
                        warn!("Unknown sender in PRIVMSG");
                        "UNKNOWN".to_owned()
                    },
                };

                match command::Command::from_str(&msg) {
                    None => (),
                    Some(cmd) => match cmd {
                        command::Command::Shutdown => return ctx.notify(message::StopSystem),
                    },
                };

                // Ignore PM message
                if *client.current_nickname() == target {
                    return;
                }

                if let Some(target) = self.mapping.get(&target) {
                    let chat_msg = message::ChatMessage {
                        target: message::ChatTarget::Guid(target.clone()),
                        from,
                        content: format_discord(msg),
                    };

                    self.subs.broadcast(chat_msg);
                }
            },
            Command::KICK(chanlist, user, _) => {
                debug!("{:?} kicked {} out of {}", from, user, chanlist);

                if user == *client.current_nickname() {
                    ctx.run_later(KICK_TIMEOUT, move |act, ctx| match act.client.as_ref().unwrap().send_join(&chanlist) {
                        Ok(_) => (),
                        Err(error) => {
                            error!("IRC: Error occured: {}", error);
                            ctx.stop();
                        },
                    });
                }
            },
            msg => debug!("Unhandled message={:?}", msg),
        }
    }
}

impl Handler<message::ChatMessage> for Irc {
    type Result = <message::ChatMessage as actix::Message>::Result;

    fn handle(&mut self, msg: message::ChatMessage, ctx: &mut Self::Context) -> Self::Result {
        let client = match self.client.as_ref() {
            Some(client) => client,
            None => {
                // Client is not available yet
                self.wait_queue.push(msg);
                return;
            },
        };

        match Self::send_msg(&client, msg) {
            Ok(_) => (),
            Err((msg, error)) => {
                warn!("IRC: Error sending message: {}", error);
                self.wait_queue.push(msg);
                ctx.stop();
            }
        }
    }
}

impl Handler<message::AddSub> for Irc {
    type Result = <message::AddSub as actix::Message>::Result;

    fn handle(&mut self, msg: message::AddSub, _: &mut Self::Context) -> Self::Result {
        self.subs.push(msg.0);
    }
}

impl Handler<message::StopSystem> for Irc {
    type Result = ();

    fn handle(&mut self, _msg: message::StopSystem, ctx: &mut Self::Context) -> Self::Result {
        warn!("IRC: Shutdown command is issued!");

        self.shutdown_client();
        ctx.stop();
    }
}

fn format_discord(msg: String) -> String {
    lazy_static! {
        static ref ACTION: regex::Regex = regex::Regex::new("^\u{1}ACTION\\s+([^\u{1}]+)\u{1}$").expect("Create ACTION regex");
        static ref ITALIC: regex::Regex = regex::Regex::new("\x1D([^\x1D]+)\x1D").expect("Create italic regex");
        static ref BOLD: regex::Regex = regex::Regex::new("(\x02[^\x02]+)\x02").expect("Create bold regex");
        static ref BOLD_ITALIC: regex::Regex = regex::Regex::new("\x1D\x02([^\x1D\x02]+)\x1D\x02").expect("Create bold italic regex");
    }

    let msg = ACTION.replace_all(&msg, "_${1}_");
    let msg = BOLD_ITALIC.replace_all(&msg, "***${1}***");
    let msg = BOLD.replace_all(&msg, "**${1}**");
    let msg = ITALIC.replace_all(&msg, "_${1}_");

    msg.to_string()
}
