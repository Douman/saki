extern crate actix;
extern crate futures;
extern crate regex;
extern crate serenity;

use std::thread;

use super::command;
use super::message;
use config;
use utils::wait_queue;

use self::futures::sync::mpsc;

#[derive(Clone)]
struct MessageHandler {
    sender: mpsc::UnboundedSender<DiscordMessage>,
}

impl serenity::prelude::EventHandler for MessageHandler {
    fn ready(&self, _ctx: serenity::prelude::Context, _bot_data: serenity::model::gateway::Ready) {
        info!("Discord: Serenity connected");
    }

    fn resume(&self, _ctx: serenity::prelude::Context, _: serenity::model::event::ResumedEvent) {
        info!("Discord: Serenity reconnected");
    }

    fn message(&self, _ctx: serenity::prelude::Context, msg: serenity::model::channel::Message) {
        // We ignore bot messages
        if msg.author.bot == true {
            return;
        }

        match self.sender.unbounded_send(msg.into()) {
            Ok(_) => (),
            Err(_) => error!("Discord: Unable to send message to actor"),
        }
    }
}

static mut QUEUE: Option<wait_queue::Queue<message::ChatMessage>> = None;

pub struct Discord {
    config: config::Discord,
    subs: super::Subscribers,
    mapping: config::MappingDiscord,
    shard_manager: Option<::std::sync::Arc<serenity::prelude::Mutex<serenity::client::bridge::gateway::ShardManager>>>,
    wait_queue: &'static mut wait_queue::Queue<message::ChatMessage>,
}

impl Discord {
    pub fn new(config: config::Discord, mapping: config::MappingDiscord) -> Self {
        let wait_queue;
        unsafe {
            if QUEUE.is_none() {
                QUEUE = Some(wait_queue::Queue::new());
            }
            wait_queue = QUEUE.as_mut().unwrap();
        }

        Self {
            config,
            subs: super::Subscribers::default(),
            mapping,
            shard_manager: None,
            wait_queue,
        }
    }

    fn inner_send(message: message::ChatMessage) -> Result<(), message::ChatMessage> {
        use self::serenity::model::id::ChannelId;
        use self::serenity::Error;
        use self::serenity::prelude::HttpError;

        let target = ChannelId(message.target.guid());

        match target.send_message(|msg| msg.content(format!("<{}> {}", message.from, message.content))) {
            Ok(_) => Ok(()),
            Err(Error::Http(HttpError::UnsuccessfulRequest(response))) => {
                //Most likely either bot is forbidden or it is request for non-existing channel.
                warn!("Discord: Message is rejected. Status: {}", response.status);
                Ok(())
            },
            //TODO: There may be other errors that do not require to restart.
            Err(error) => {
                error!("Discord: Unable to send message. Error: {:?}", error);
                Err(message)
            }
        }
    }

    fn send_message(&mut self, ctx: &mut actix::Context<Self>, msg: message::ChatMessage) {
        use actix::ActorContext;

        if !self.is_started() {
            return self.wait_queue.push(msg);
        }

        match Self::inner_send(msg) {
            Ok(_) => (),
            Err(msg) => {
                self.wait_queue.push(msg);
                ctx.stop();
            }
        }
    }

    fn is_started(&self) -> bool {
        self.shard_manager.is_some()
    }

    fn shutdown_discord_worker(&mut self) {
        if let Some(shard_manager) = self.shard_manager.take() {
            shard_manager.lock().shutdown_all();
        }
    }
}

impl actix::Actor for Discord {
    type Context = actix::Context<Self>;

    fn stopping(&mut self, _ctx: &mut Self::Context) -> actix::Running {
        info!("Discord: Stopping");

        self.shutdown_discord_worker();

        actix::Running::Stop
    }

    fn started(&mut self, ctx: &mut actix::Context<Self>) {
        use actix::{ActorFuture, AsyncContext, WrapFuture, ActorContext};

        info!("Discord: Starting");

        let (sender, receiver) = futures::sync::mpsc::unbounded();
        let token = self.config.token.clone();
        let handler = MessageHandler { sender };

        let (manager_sender, manager_recv) = futures::sync::oneshot::channel();
        thread::Builder::new()
            .name("discord-worker".to_owned())
            .spawn(move || {
                let mut client = serenity::client::Client::new(&token, handler.clone()).expect("Error creating client");
                manager_sender.send(client.shard_manager.clone()).expect("To send shard_manager");

                match client.start_autosharded() {
                    Ok(_) => info!("Discord: Client shuted down"),
                    Err(why) => warn!("Discord: Finished with error: {}", why),
                }
            })
            .expect("To create worker thread");

        ctx.add_stream(receiver);
        let manager_recv = manager_recv
            .into_actor(self)
            .map(|manager, act, _ctx| {
                act.shard_manager = Some(manager);

                for msg in act.wait_queue.drain() {
                    let _ = Self::inner_send(msg);
                }
            })
            .map_err(|_, _act, ctx| {
                warn!("Discord: Couldn't get shard manager. Cancelled future");
                ctx.stop();
            });

        ctx.spawn(manager_recv);
    }
}

impl actix::Supervised for Discord {
    fn restarting(&mut self, _: &mut actix::Context<Self>) {
        info!("Discord: Restarting");
    }
}

#[derive(Debug)]
struct DiscordMessage(pub serenity::model::channel::Message);
impl From<serenity::model::channel::Message> for DiscordMessage {
    fn from(msg: serenity::model::channel::Message) -> DiscordMessage {
        DiscordMessage(msg)
    }
}
impl From<DiscordMessage> for serenity::model::channel::Message {
    fn from(wrapper: DiscordMessage) -> serenity::model::channel::Message {
        wrapper.0
    }
}

impl actix::Message for DiscordMessage {
    type Result = ();
}

impl actix::StreamHandler<DiscordMessage, ()> for Discord {
    fn error(&mut self, _error: (), _ctx: &mut Self::Context) -> actix::Running {
        warn!("Discord: Error handling messages");
        actix::Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        use actix::ActorContext;

        warn!("Discord: Message stream is closed");
        ctx.stop();
    }

    fn handle(&mut self, msg: DiscordMessage, ctx: &mut Self::Context) {
        use actix::AsyncContext;

        let msg = msg.0;

        match command::Command::from_str(&msg.content) {
            None => (),
            Some(cmd) => match cmd {
                command::Command::Shutdown => return ctx.notify(message::StopSystem),
            },
        };

        let channel_id = msg.channel_id.0;

        debug!("Discord: {:?}", &msg);
        if let Some(target) = self.mapping.get(&channel_id.to_string()) {
            let content = msg.content.replace("\n", " ").trim().to_owned();
            let from = msg.author.name;
            let target = message::ChatTarget::Text(target.clone());

            if content.len() != 0 {
                let chat_msg = message::ChatMessage {
                    target: target.clone(),
                    from: from.clone(),
                    content: format_irc(content),
                };
                self.subs.broadcast(chat_msg);
            }

            for attach in msg.attachments {
                let chat_msg = message::ChatMessage {
                    target: target.clone(),
                    from: from.clone(),
                    content: attach.url,
                };
                self.subs.broadcast(chat_msg);
            }
        }
    }
}

impl actix::Handler<message::AddSub> for Discord {
    type Result = <message::AddSub as actix::Message>::Result;

    fn handle(&mut self, msg: message::AddSub, _: &mut Self::Context) -> Self::Result {
        self.subs.push(msg.0);
    }
}

impl actix::Handler<message::ChatMessage> for Discord {
    type Result = <message::ChatMessage as actix::Message>::Result;

    fn handle(&mut self, msg: message::ChatMessage, ctx: &mut Self::Context) -> Self::Result {
        self.send_message(ctx, msg);
    }
}

impl actix::Handler<message::StopSystem> for Discord {
    type Result = ();

    fn handle(&mut self, _msg: message::StopSystem, ctx: &mut Self::Context) -> Self::Result {
        use actix::ActorContext;
        warn!("Discord: Shutdown command is issued!");

        ctx.stop();
    }
}

fn format_irc(msg: String) -> String {
    lazy_static! {
        static ref ITALIC: regex::Regex = regex::Regex::new("_([^_]+)_").expect("Create italic regex");
        static ref BOLD: regex::Regex = regex::Regex::new("\\*\\*([^\\*]+)\\*\\*").expect("Create bold regex");
        static ref BOLD_ITALIC: regex::Regex = regex::Regex::new("\\*\\*\\*([^\\*]+)\\*\\*\\*").expect("Create bold italic regex");
    }

    let msg = BOLD_ITALIC.replace_all(&msg, "\x02\x1D${1}\x1D\x02");
    let msg = BOLD.replace_all(&msg, "\x02${1}\x02");
    let msg = ITALIC.replace_all(&msg, "\x1D${1}\x1D");

    msg.to_string()
}
