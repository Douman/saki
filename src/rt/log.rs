extern crate cute_log;
extern crate lazy_panic;

/// Initializes logging facilities
pub fn init() {
    set_panic_message!(lazy_panic::formatter::Simple);
    cute_log::init().expect("To initialize log");
}
